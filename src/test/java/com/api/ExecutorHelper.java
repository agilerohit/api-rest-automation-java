package com.api;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

    public class ExecutorHelper extends UtilityHelper {

	public HttpClient client;
	public static String url;
	protected static HttpResponse response;

	public ExecutorHelper(String url) {
		this.url = url;
		client = HttpClientBuilder.create().build();
	}

	// Method to get api path along with header
	  public void getRequest(String path) throws ClientProtocolException, Throwable {
	  HttpGet request = new HttpGet(url + path);
	  StringBuffer responseString = new StringBuffer();

		// Generating headers
	  Map<String, String> reqHeaders = getHeaders();
	  reqHeaders.forEach((key, value) -> {
	  request.setHeader(key, value);
			/*
			 * System.out.println("Key : " + key); System.out.println("value : "
			 * + value);
			 */
		});

		response = client.execute(request);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
			responseString.append(line);
		}

		System.out.println(response.getStatusLine().getStatusCode());
		int code = response.getStatusLine().getStatusCode();
		System.out.println(code);
		if (code == 200) {
		ExcelUtils.updateExcelFile(1, 3);
		}

		// System.out.println(responseString.toString());

		// Print Response Header in key Value pair
		getResponseHeaders();
		getRespnseBody(responseString);

	    }

	 // Post api along with data
	    public void postMethod(String path, String methodbody) throws ClientProtocolException, IOException {

		HttpPost post = new HttpPost(url + path);

		// Generating headers
		Map<String, String> reqHeaders = getHeaders();
		reqHeaders.forEach((key, value) -> {
			post.setHeader(key, value);
			System.out.println("Key : " + key);
			System.out.println("value : " + value);
		});

		StringBuffer responseString = new StringBuffer();
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
		responseString.append(line);
		}

		System.out.println(response.getEntity().getContent());
		System.out.println(response.getStatusLine().getStatusCode());
		System.out.println(response.getStatusLine().getReasonPhrase());
		System.out.println(responseString.toString());
	    }

	    // Method to get login token
	    public void getToken(String path, LinkedHashMap<String, String> loginheader, ArrayList<NameValuePair> nameValuePair) throws ClientProtocolException, Throwable {

		HttpPost post = new HttpPost(url + path);
		StringBuffer responseString = new StringBuffer();

		// Set parameters for method body
		nameValuePair = new ArrayList<NameValuePair>();
		nameValuePair.add(new BasicNameValuePair("username", "rohitclient"));
		nameValuePair.add(new BasicNameValuePair("password", "strange!"));
		nameValuePair.add(new BasicNameValuePair("grant_type", "password"));
		nameValuePair.add(new BasicNameValuePair("client_id", "daecdcd831e34800f061af1918f03dae"));
		post.setEntity(new UrlEncodedFormEntity(nameValuePair));

		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		while ((line = rd.readLine()) != null) {
		responseString.append(line);
		}

		// System.out.println(response.getStatusLine().getStatusCode());
		// System.out.println(response.getStatusLine().getReasonPhrase());

		// Print response body in string form
	     System.out.println(responseString);

		// Convert response body from String to key : value pair
		Map<Object,Object> responsebody = new HashMap<Object, Object>();
		JSONObject jObject = new JSONObject(responseString.toString());
		Iterator<?> keys = jObject.keys();
		String accessToken = "";
		while (keys.hasNext()) {
		String key = (String) keys.next();
		if (key.equals("access_token")) {
		accessToken = jObject.getString(key);
		}
		//String value =jObject.getString((String) key);
		responsebody.put(key, jObject.get(key));
		}
		
		final String projectDir = System.getProperty("user.dir");
		final String filepath = projectDir + "\\PropertyFile.properties";
		System.out.println("file : " + filepath);
		FileInputStream in = new FileInputStream("PropertyFile.properties");
		Properties props = new Properties();
		props.load(in);
		in.close();
		props.setProperty("Authorization", "bearer " + accessToken);
		FileOutputStream out = new FileOutputStream("PropertyFile.properties");
		props.store(out, "\n");
		out.close();

	    }

	    private Map<String, String> getHeaders() throws IOException {

		HashMap<String, String> headers = new HashMap<String, String>();
		FileInputStream in = new FileInputStream("PropertyFile.properties");
		Properties props = new Properties();
		props.load(in);
		for (String key : props.stringPropertyNames()) {
			headers.put(key, props.getProperty(key));
		}
		// print request header
		// System.out.println(headers);
		return headers;
	    }

	   //Get Response header
	    private void getResponseHeaders() throws IOException {

		Header[] headers = response.getAllHeaders();
		for (Header header : headers) {
		System.out.println("Key : " + header.getName() + " ,Value : " + header.getValue());

		}

	    }

	// Get Method body in key value pair
	   public void getRespnseBody(StringBuffer responseString) throws IOException, Exception {
		// print response body in string form
		// System.out.println(responseString.toString());
		JSONObject jObject = new JSONObject(responseString.toString());
		HashMap<Object, Object> mp = new HashMap<Object, Object>();
		Iterator<?> keys = jObject.keys();
		while (keys.hasNext()) {
		String key = (String) keys.next();
//		String value = jObject.valueToString(key)
		mp.put(key, jObject.get(key));
		}

		mp.forEach((key, value) -> {
		// print response body in key value form
		System.out.println("Body in key pair : " + key);
		System.out.println("Body in value pair : " + value);
		});

	}

}

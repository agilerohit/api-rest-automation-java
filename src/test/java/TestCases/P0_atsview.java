package TestCases;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;

import com.api.ExecutorHelper;
import com.api.UtilityHelper;

public class P0_atsview extends ExecutorHelper{

public P0_atsview(String url) {
super(url);
}

@Test
public void AccessAtsapplication() throws ClientProtocolException, Throwable
{
	
P0_atsview obj = new P0_atsview("https://stage.upwork.com");
obj.getToken("/api/v3/oauth2/token/", UtilityHelper.loginheader(), new ArrayList<NameValuePair>());	
}

}

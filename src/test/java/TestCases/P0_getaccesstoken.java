package TestCases;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.testng.annotations.Test;

import com.api.*;


public class P0_getaccesstoken extends ExecutorHelper{
	
public P0_getaccesstoken(String url) {
super(url);
}

@Test
public void fetchlogintoken() throws ClientProtocolException, Throwable
{
	
P0_getaccesstoken obj = new P0_getaccesstoken("https://stage.upwork.com");
obj.getToken("/api/v3/oauth2/token/", UtilityHelper.loginheader(), new ArrayList<NameValuePair>());	
		
}	

}
